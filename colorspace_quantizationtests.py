from skimage.color import rgb2lab, rgb2gray, lab2rgb, lab2xyz, xyz2rgb
import numpy as np
import sklearn.cluster as cluster
import matplotlib.pyplot as plt
import cv2
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import ConvexHull, convex_hull_plot_2d
from sklearn.neighbors import NearestNeighbors

ab_vals = np.arange(-110, 110)
a, b = np.meshgrid(ab_vals, ab_vals)

L = 50 * np.ones_like(a)
acc = np.zeros(shape=(len(a[0]), len(a[0]), 3))
acc[:,:,0] = L
acc[:,:,1] = a
acc[:,:,2] = b
ab_grid = acc[:,:,1:3]
x, y, z = ab_grid.shape
ab_grid = ab_grid.reshape(x*y, z)

acc = (lab2rgb(acc)*255).astype('int32')

#plt.imshow(acc)
#plt.show()
pts = np.load('utils/pts_in_hull.npy')
plt.scatter(ab_grid[:,0], ab_grid[:,1])
plt.scatter(pts[:,1], pts[:,0], color='r')
plt.xlabel('b')
plt.ylabel('a')
plt.title('Convex hull on top of ab grid')
plt.show()
