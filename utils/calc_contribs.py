import sys
import numpy as np
import os
import skimage.io
import sklearn.neighbors as neighbors

points = './resources/pts_in_hull_imagenet.npy'
directory = '/home/nasdenkov/colorization_data/data/rgb/all'

def priors():
    gridpoints = np.load(points)
    priors = np.zeros(gridpoints.shape[0], dtype='float64')
    nn = neighbors.NearestNeighbors(n_neighbors=5, algorithm="ball_tree").fit(gridpoints)
    for filename in os.listdir(directory):
        if filename.endswith(".png"):
           # print("processing " + filename)
            img = skimage.io.imread(os.path.join(directory, filename))
            lab = skimage.color.rgb2lab(img)
            lab_flat = lab.reshape((256*256, 3))
            assert ((lab_flat[0] == lab[0][0]).all())
            assert ((lab_flat[1] == lab[0][1]).all())
            ab = lab_flat[:, 1:3]
            (dist, inds) = nn.kneighbors(ab)

            # smallest distances map to largest contributions, but dist can be 0
            dist = dist + 0.0000001
            inv_dists = 1 / dist
            contribs = inv_dists / np.sum(inv_dists, 1).reshape(-1, 1)
            assert (((np.sum(contribs, 1) < 1.1).all() and (np.sum(contribs, 1) > 0.9).all()))
            np.add.at(priors, inds.reshape(-1), contribs.reshape(-1))

    np.save("../resources/prior_contribs", priors)

priors()