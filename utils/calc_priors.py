import numpy as np

hull = './resources/pts_in_hull_imagenet.npy'
contribs = './resources/prior_contribs.npy'

def priors():
    pr = np.load(contribs)
    print(pr / np.sum(pr))
    hp = np.load(hull)
    id = (pr.argsort()[-5:])
    print(id)
    print(hp[id])
    print(sum(pr))

    print("Unused: ")
    print(np.sum(pr < 1))
    indx = (pr > 1) # The contribs g.t 1 are the ones that have been updated at least two times
    pts_hull = hp[indx]
    priors = pr[indx] / np.sum(pr[indx])

    print(np.sum(priors))
    print(priors)
    # There are only 226 points in the hull that actually map to any of the colors used in my dataset
    np.save("/home/nasdenkov/PycharmProjects/colorful-image-colorization/resources/pts_in_hull_mydataset", pts_hull)
    # The same thing applies for the priors
    np.save("/home/nasdenkov/PycharmProjects/colorful-image-colorization/resources/prior_probs_mydataset", priors)

    print(len(priors))

    priors = pr / np.sum(pr)
    np.save("/home/nasdenkov/PycharmProjects/colorful-image-colorization/resources/train_priors", priors)
    print(np.sum(priors))

priors()