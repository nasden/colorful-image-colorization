import numpy as np
from data_loader import TrainImageFolder
import torch
from torchvision import transforms
"""
THIS UTILITY SCRIPT IS NOT MY OWN !
COPYRIGHT -> 
    https://github.com/richzhang/colorization/blob/815b3f7808f8f2d9d683e9ed6c5b0a39bec232fb/interactive-deep-colorization/caffe_files/util.py
"""
def check_value(inds, val):
    # Check to see if an array is a single element equaling a particular value
    # Good for pre-processing inputs in a function
    if(np.array(inds).size==1):
        if(inds==val):
            return True
    return False


def flatten_2d_array(arr, axis = 1):
    """
    Flattens a ndim array into a 2d array with a given axis
    n0xn1x...xnn -> productof(n, naxis)xnaxis
    """
    shapearr = np.array(arr.shape)
    dims = arr.dim()
    nonaxis_indices = np.setdiff1d(np.arange(0, dims), np.array((axis))) # contains the indices of the axes != axis
    num_points = np.prod(shapearr[nonaxis_indices]) # Calculates the product of the values inside the axes != axis
    axisorder = tuple(np.concatenate((nonaxis_indices, np.array(axis).flatten()), axis=0).tolist()) # axis shape
    flatpoints = arr.permute(axisorder) # Permutes the array based on the new axis shape
    flatpoints = flatpoints.contiguous().view(num_points.item(), shapearr[axis].item()) # Reshapes to match needed form
    return flatpoints


def unflatten_2d_array(pts_flt,pts_nd,axis=1,squeeze=False):
    """
    Unflattens a 2d array with a certain axis
    prod(N, n_axis) x M -> n0xn1x....xNd
    """
    NDIM = pts_nd.dim()
    SHP = np.array(pts_nd.shape)
    nax = np.setdiff1d(np.arange(0,NDIM),np.array((axis))) # non axis indices
    NPTS = np.prod(SHP[nax])

    if(squeeze):
        axorder = nax
        axorder_rev = np.argsort(axorder)
        M = pts_flt.shape[1]
        NEW_SHP = SHP[nax].tolist()
        pts_out = pts_flt.reshape(NEW_SHP)
        pts_out = pts_out.transpose(axorder_rev)
    else:
        axorder = np.concatenate((nax,np.array(axis).flatten()),axis=0)
        axorder_rev = np.argsort(axorder)
        M = pts_flt.shape[1]
        NEW_SHP = SHP[nax].tolist()
        NEW_SHP.append(M)
        pts_out = pts_flt.reshape(NEW_SHP)
        pts_out = pts_out.transpose(axorder_rev)

    return pts_out


def na():
    return np.newaxis

#original_transform = transforms.Compose([
#    transforms.RandomCrop(224),
#    transforms.RandomHorizontalFlip(),
#])


#train_set = TrainImageFolder("/home/nasdenkov/colorization_data/data/rgb/train", original_transform)
#data_loader = torch.utils.data.DataLoader(train_set, batch_size = 1, shuffle = True, num_workers = 4)
#for i, (images, img_ab) in enumerate(data_loader):
#    res = flatten(img_ab[0])
#    print(res.size(), img_ab.size())