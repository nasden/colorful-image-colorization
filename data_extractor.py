import cv2
import PIL.Image as Image
import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import os

class VideoProcessor():
    """""
    Processed all the videos and generated images that were used for training
    """
    def __init__(self, video, width, height):
        self.video = video
        self.width_per_img = width
        self.height_per_img = height
        self.previous_image = None

    def downsample_image(self, image, verbose = False):
        img = Image.fromarray(image).resize((self.width_per_img, self.height_per_img), Image.ANTIALIAS)
        img = np.array(img)
        if verbose is True:
            print("Showing image before downsample...")
            plt.imshow(image)
            print("Showing image after downsample...")
            plt.imshow(img)
            plt.show()
        return img

    def convertToGrayScale(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return gray

    def frameIsUnique(self, imageA, imageB, threshold):
        diff = metrics.mean_squared_error(imageA, imageB)
        return diff > threshold

    def generate_Lab(self, source, destination):

        for file in os.listdir(source):
            img = cv2.imread(os.path.join(source, file)) # With cv2, images are always BGR
            img = np.array(img)
            helper = cv2.cvtColor(img, cv2.COLOR_BGR2Lab)
            cv2.imwrite(os.path.join(destination, file), helper)

    def video_to_frames(self, output_name):
        video_capture = cv2.VideoCapture(self.video)
        count = 0
        while video_capture.isOpened():
            success, image = video_capture.read()
            if success:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                img = self.downsample_image(image, verbose = False)
                if self.previous_image is None:
                    self.previous_image = img
                imageA = self.convertToGrayScale(img)
                imageB = self.convertToGrayScale(self.previous_image)
                if not self.frameIsUnique(imageA, imageB, 30):
                    continue
                else:
                   # plt.imshow(img)
                   # plt.show()
                    cv2.imwrite(output_name + "%d.png" % count, cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
                    count += 1
                    self.previous_image = img
            else:
                break
        cv2.destroyAllWindows()
        video_capture.release()
