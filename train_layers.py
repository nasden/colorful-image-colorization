import torch
import numpy as np
import torch.nn.functional as F
import os
from torchvision import transforms
import sklearn.neighbors as nn
from skimage.transform import resize
from skimage import color
import warnings
from utils.utils_trainlayers import check_value, flatten_2d_array, unflatten_2d_array, na
from numpy import inf

class QuantizedEncLayer(object):
    """
    This layer encodes the (a, b) pairs coming from an image to a specific color using the
    valid color points which are mapped inside the sRGB convex hull as training data to feed a k-NN classifier.
    For more information, read colorization-doc.md

    Output: NxQ
    """

    def __init__(self):
        self.n_neighbors = 5
        self.sigma = 0.5 # Determines the width of the kernel. Check Gaussian Kernel smoothing for more details
        self.PTS_DIR = './resources/'
        self.cn = ColorEncoder(n_neighbors=self.n_neighbors, sigma=self.sigma, pts_path=os.path.join(self.PTS_DIR, 'pts_in_hull_imagenet.npy'))
        self.X = 224
        self.Y = 224
        self.Q = self.cn.K

    def forward(self, X):
        enc = self.cn.encode_points(X)
        max_enc = np.argmax(enc, axis = 1).astype('int')
        return enc, max_enc


class PriorBoostLayer(object):
    """
    Layer boosts ab values based on their rarity
    """
    def __init__(self, PRIORS_DIR= './resources', gamma = 0.5, alpha = 1.):
        self.PRIORS_DIR = PRIORS_DIR
        self.gamma = gamma # Factor that controls the mixing between the prior distribution and the uniform distribution
        self.alpha = alpha # The exponent of the rebalance term
        self.pc = PriorFactor(alpha = self.alpha, gamma = self.gamma, priors_file = os.path.join(self.PRIORS_DIR, "train_priors.npy"))

    def forward(self, X, axis = 1):
        return self.pc.forward(X, axis = axis)

class NonGrayMaskLayer(object):
    """
    Acknowledgement: This class implementation is taken from the original paper implementation of Zhang. et. al
    It returns a 1 if the passed in image is NOT only gray. This is done so that when backpropagating, the 1 gets multiplied with the
    boost factors and they get passed on to the Encoding Layer for further propagation. However, if the image is purely gray, then we pass in 0
    thus a boost factor is not applied.
    """
    def __init__(self):
        self.threshold = 5.

    def forward(self, X):
        X = X.cpu().data.numpy()
        a = (np.sum(np.sum(np.sum((np.abs(X) > self.threshold).astype('float'), axis=1), axis=1), axis=1) > 0)[:,na(), na(), na()].astype('float')
        return a

class PriorFactor(object):
    """
    Handles the computations for the class rebalancing term
    """
    def __init__(self, alpha, gamma = 0., priors_file=''):
        self.alpha = alpha
        self.gamma = gamma
        self.prior_probs = np.load(priors_file) # This is the generated empirical prior probability over the training set

        # We need to define a uniform probability distribution to mix with the prior one
        self.uniform = np.zeros_like(self.prior_probs)
        self.uniform[self.prior_probs != 0] = 1.
        self.uniform /= np.sum(self.uniform)

        # The convex combination between the prior and uniform distributions
        # As you can see, gamma controls how much of both distributions are mixed
        self.mix = (1 - self.gamma) * self.prior_probs + self.gamma * self.uniform

        # Prior factor = w
        # Essentially generates a factor for the rarest colors in the prior distribution
        self.prior_factor = self.mix ** -self.alpha
        self.prior_factor[self.prior_factor == inf] = 0.
        self.prior_factor /= np.sum(self.prior_probs * self.prior_factor) # Normalize to match E = 1

        self.implied_prior = self.prior_probs * self.prior_factor
        self.implied_prior = self.implied_prior / np.sum(self.implied_prior) # Normalize to match E = 1



    def forward(self, x, axis = 1):
        """
        We extract the indices of the maximum values of the encoded feature map.
        They essentially are the smallest euclidean distances from their neighbors inside the hull.
        These distances receive a factor that prioritizes them when backpropagating
        """
        maxindices = np.argmax(x, axis = axis)
        factor = self.prior_factor[maxindices]
        if axis is 0:
            return factor[na(), :]
        elif axis is 1:
            return factor[:, na(), :]
        elif axis is 2:
            return factor[:, :, na(), :]
        elif axis is 3:
            return factor[:, :, :, na()]


class ColorEncoder(object):
    """
    Helper class for QuantizedEncLayer. Encodes points using k-NN and Gaussian kernel
    """
    def __init__(self, n_neighbors, sigma, pts_path, pts=-1):
        if check_value(pts, -1):
            self.pts = np.load(pts_path)
        else:
            self.pts = pts

        self.K = self.pts.shape[0] # 313
        self.n_neighbors = int(n_neighbors) # Defines the number of neighbours to use for k-NN
        self.sigma = sigma
        self.nbrs = nn.NearestNeighbors(n_neighbors = n_neighbors, algorithm = 'ball_tree').fit(self.pts) # train k-NN with convex hull data
        self.used = False

    def encode_points(self, pts, axis = 1, sameblock = True):
        flat_points = flatten_2d_array(pts, axis = axis) # Split image abspace into array of (a, b) pairs
        P = flat_points.shape[0] # Number of (a, b) pairs
        if (self.used and sameblock):
            self.enc_flat_points[...] = 0
        else:
            self.used = True
            self.enc_flat_points = np.zeros((P, self.K)) # (3136, N) # For each (a, b) pair -> color hist of N (points in hull) values
            self.p_indices = np.arange(0, P, dtype='int')[:, na()] # (3136, 1)

        P = flat_points.shape[0]

        (eucl_distances, indices) = self.nbrs.kneighbors(flat_points) # (3136, k) -> Finds the k-NN of the passed in ab points

        # Apply gaussian kernel to normalize euclidean distances in range [0,1]
        gauss_wts = np.exp(-eucl_distances ** 2 / (2 * self.sigma ** 2))
        gauss_wts /= np.sum(gauss_wts, axis = 1)[:, na()]
        self.enc_flat_points[self.p_indices, indices] = gauss_wts

        enc_nd_points = unflatten_2d_array(self.enc_flat_points, pts, axis = axis)
        return enc_nd_points

    def decode_points(self, enc_nd_points, axis = 1):
        enc_flat_points = flatten_2d_array(enc_nd_points, axis = axis)
        dec_flat_points = np.dot(enc_flat_points, self.pts) # Summing over the bins' ab values, weighted by the hist value
        dec_nd_points = unflatten_2d_array(dec_flat_points, enc_nd_points, axis = axis)
        return dec_nd_points
