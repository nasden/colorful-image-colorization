# Colorful Image Colorization - Documentation by Atanas Denkov

In this document you will find the necessary steps that I took to train a ConvNet that colorizes grayscale images. 

## 1. The Dataset
In the original paper, the network was trained on one million images from `ImageNet`, which proves to be too challenging for the scope of this project, due to the lack of performance on the machine executing the computations. Therefore, I chose to work on a smaller dataset that is derived from a Czech cartoon TV program called `O loupežníku Rumcajsovi`, which aired between 1967 and 1974. I extracted the dataset by writing a Python script which iterates over each video frame of the cartoon. The script is called `data_extractor.py` and can be found in the Git repository.

### 1.1 Method of extraction
Since the origin of the data set is a cartoon TV program, several problems surround the extraction of the images. <br/>
First, the videos run at 25 frames per second. You can imagine that in <b>one</b> second, the 25 processed frames don't differ very much from one another, thus the information that they provide is not very 'unique'. This means that a large amount of the frames needs to be filtered out.<br/>
Secondly, the videos contain opening and closing credits that largely contain a light blue color in the background (roughly 80% of the whole image), which do not provide any necessary information to the network at all. In fact, if the credits do not get filtered out, the behaviour of the network would change, thus rendering predictions with a highly emphasized blue channel because the credits result to 4% of the whole dataset.<br/>
As a solution, I used simple squared mean thresholding, acting on consecutive frames extracted from the movie:<br/>
```python
 def frameIsUnique(self, imageA, imageB, threshold):
        diff = metrics.mean_squared_error(imageA, imageB)
        return diff > threshold
```
Via this function, I determined if `imageA` differs from `imageB` by a large enough threshold to classify it as unique. If it did, then the next frame that was loaded was saved into `imageB` and compared with `imageA` again. This way, a new frame was only compared with the previous frame that was classified as unique and not with every frame beforehand. The threshold parameter is a hyperparameter and needed to be tuned a bit extra to find an appropriate value. This method manages to remove a large amount of the credit frames as well, but not all of them.<br/>
For removing the rest of the credit frames, I used a tool called `PySceneDetect` that provides scene detection algorithms. I ran one of those algorithms against all the videos and split them into the appropriate scenes, respectively. Since these algorithms use similar but more complex methods to differentiate the scenes, they were able to correctly separate the remaining opening and closing credits from the actual episodes. That allowed me to disregard the scenes that contained only the credits and only use the remaining videos to extract the images.

### 1.2 Training and testing datasets
The total dataset I used consists of 28 episodes. After processing, they were rendered to a total of 32671 images. 2666 of those images were used as test data and the rest as training data. Note, that the test data cannot be determined randomly. If I were to pick random images, the likelihood that they are very close to, or nearly identical to some of the images in the training set is pretty high, which would result in poor measurements when tracking the generalization capabilities of the network. Therefore, the test dataset was derived from two separate episodes that were never seen by the network in the training process.<br/>
I think it is important to note, that the total set of images should be considered a bit generous because the meaningful magnitude of the set is actually much smaller, as most images contain simple modifications of the previously seen frames.

<b>I hereby acknowledgle that the cartoons were used for educational purposes only and that the copyright belongs to Česká televize. </b><br/>

## 2. Problem statement and approach
Consider an image <i>I</i> &nbsp;represented in the RGB color space. This image can be broken down into three major components: <br/>
><b>L</b> - Lightness of the image - black is represented when <b>L = 0</b><br> and white when <b>L = 100</b><br/>
<b>a</b> -  &nbsp; <b>-128(green) <= a < 128(red)</b><br/>
<b>b</b> -  &nbsp; <b>-128(blue) <= a < 128(yellow)</b>
 
To access these components, <i>I</i> &nbsp;needs to be converted to the <b>CIELab</b> color space. Afterwards, the problem at hand can be defined the following way: <br/>

> Given the <b>L</b> channel of the image <i>I</i>, &nbsp;predict the <i>a</i>&nbsp;and <i>b</i> &nbsp;channels of <i>I</i>.

<br>
The <b>CIELab</b> color space is considered to be a more adequate approach when dealing with this problem because the distribution of colors matches the human perception of those colors. Meaning that the Euclidian distance between two colors in this color space is approximated to match their distance when measured by the human eye.

### 2.1 Color channel estimations
When estimating the a and b channels,  there are several possible approaches to choose from.  The choice can significantly affect the resulting visual colorization, training time or final CNN performance.<br/>
A  straightforward  option  would  be  to  estimate  the  values  of  a  and  b  for  each  pixel directly.   The  advantage  of  this  approach  is  the  simplicity,  both  in  terms  of  reasoning and implementation.  It provides the option of using a simple loss functions, such as L2 Euclidean norm, which speeds up training.<br/>
The problem with this approach is that it doesn't handle multimodality well. If an object can take on a range of different a and b values (e.g a ball can take on any color, regardless of what the actual ground truth color is), the Euclidian loss will tend to average towards zero, thus producing desaturated and unsatisfying results. 
<br>
A completely different approach, and the one I am using in this project, is to pose the task as a classification problem. I do this by estimating a color histogram for every pixel in the target image. This color histogram, in its core, is a probability distribution predicted by the network for each pixel, since the final layer uses the softmax activation function that normalizes the output to form a proper distribution. 

#### 2.1.1 Classification approach
The first step would be to define a canonical set of colors to predict. This was quite difficult and took a couple of days to understand because the original paper doesn't mention at all how the next steps should be executed. <br/>
As in the original paper by Zhang. et al, I was supposed to quantize the ab grid space into evenly spaced bins, taking a point every 10 units of the grid and remove any colors that do not map inside the sRGB gamut. This leaves us with 313 proper ab pairs which can be used.

> Side note: Even though the a and b channels take in values from the range -128 to 128, the values positioned at the extremes of this range are not visible in the sRGB gamut. Therefore, I discard them and consider values in the range of -110 to 110.

So, in order to to 'quantize' the ab grid space, one should generate all possible combinations of values inside that space. This can be done with the following code snippet:
```python
ab_vals = np.arange(-110, 110)
a, b = np.meshgrid(ab_vals, ab_vals)

L = 50 * np.ones_like(a)
acc = np.zeros(shape=(len(a[0]), len(a[0]), 3))
acc[:,:,0] = L
acc[:,:,1] = a
acc[:,:,2] = b
```
`meshgrid` generates a grid with all possible compinations of integer values inside the range -110 to 110. We then define a constant lightness value for the L channel, which is 50, and save all values inside the `acc` variable. If we were to convert this variable to take in RGB values and plot it, we'll get the following image:<br><br>
![](resources/abspace.png)
<br><br>
This essentially means that we have all the possible a and b combinations stored in a matrix. Fortunately, Zhang et. al provides a numpy file, which can get loaded inside a python script. This file contains the convex hull coordinates of all (a, b) pairs in the a b space that are in-gamut. So if I were to plot the convex hull coordinates over the a b grid, it would look something like this: <br>

![](resources/hullovergrid.png)
<br><br>
Those red dots are what interest me, because they are the in-gamut quantized form of the a b space or in other words, they are the valid colors that will get probabilisticly mapped to the target. The file is `./resources/pts_in_hull.npy` <br>

The next step would be to convert a given value from ab to a color histogram, we find k-nearest-neighbours values in this quantized set and create a combination that closely matches the original value (proportional to the distances from the neighbours). Furthermore, this ”sharp” histogram is smoothed with a Gaussian kernel (σ = 5) to speed up CNN training  convergence. This process is very similar to color quantization, where e.g the k-means clustering algorithm is applied to produce a small set of colors that represent every color in the original image.<br>
><div style="text-align:center"><img src="resources/gausskernelformula.png" /></div>I wondered why the Gaussian kernel increases training convergence ? The Gaussian kernel is a non-linear function of <b>Euclidean distance</b> ! This means that as the euclidean distance <b>increases</b>, the kernel function <b>decreases</b>. Combined with the nice property that the Gaussian function is bound between 0 and 1, modeling with this kernel provides stability and leads to faster convergence rates.  

To achieve this functionality, I created a class called `ColorEncoder` which is located in the script `train_layers.py`. To initialize this class, the number of neighbours for the k-NN algorithm, the path to the values of the sRGB convex hull and the std. deviation for the Gaussian kernel need to be specified. The k-NN algorithm then gets trained with the in-gamut colors. This class provides the function `encode_points` which will later be used in the forward pass of the first layer in the network. This function takes in the ab pairs of an input image and estimates the k nearest neighbors for each of these pairs. Afterwards, the Gaussian kernel is applied to the estimated euclidean distances for each pair, the dimensions get restored and the result gets passed to the next layer. The forward pass is implemented in the class `QuantizedEncLayer`, which uses the `ColorEncoder` as a helper to apply the k-NN and the Gaussian kernel. The class `ColorEncoder` also provides the function `decode_points` which generates (a, b)-pairs from  the color histogram by summing over the bins' ab values and weighting them by the histogram value. This method is called the `Expectation` method and in code it is simply a dot product between the flattened encoded points and the points inside the convex hull. To clarify, both these functions give us a way to generate color histograms from (a, b) pairs, which can be used as probability distributions, and convert these histograms back to (a, b) pairs that contain color approximations based on the nearest in-gamut neighbors of those pairs.


### 2.2 Loss function
Regardless of the output color representation, we require a loss function to measure prediction errors and obtain meaningful gradients to update the network's weights. 
Since the color histograms are closely related to probability distributions, I will use the Kullback-Leibler divergence function as a natural distance function of two probability distributions.
Additionaly, the loss function should be enhanced by using a prior-boosting reweighting factor - a technique called class rebalancing and used by Zhang et al. This enhancement is needed because the distribution of (a, b) values in most images strongly favors specific (a, b) values due to the presence of backgrounds such as blue sky, green grass etc. that take up significant proportions of the images. This renders more vibrant colors less probable, therefore a rebalancing factor is included in the loss, which ends up looking like this:
<div style="text-align:center"><img src="resources/loss.png" /></div>
where <i>v</i>&nbsp; is (function of the ground truth color histogram) the class rebalancing factor. <i>P</i>&nbsp; is the ground truth color histogram with specified height <i>h</i> and width <i>w</i> and <i>P^</i>&nbsp; is the predicted color histogram. 

#### 2.2.1 Rebalancing factor
We already have a bit of information regarding the data. We know that desaturated blue and green (a, b) pairs are more probable. This corresponds to the criteria needed to build an empirical prior distribution before actually seeing any data, since we believe that specific values for the pairs are in a given range (the range depicting the aforementioned desaturated values of a and b). This allows us to build an empirical prior distribution for the 313 possible colors, considering that most probably the values will be desaturated blue/green values. We will call this prior distribution <i>p~</i>. We also define a term gamma - <i>s</i>&nbsp;, which is used to create a mixture between a uniform distribution and <i>p~</i>. This is done to ensure that the loss function doesn't favor one color over another disproportionately. As we know mathematically, a uniform distribution contains values with even probabilities of occuring, thus, when combined with the prior distribution, the outcome will be more generalizable. Having both <i>s</i> and <i>r</i>, we can define the class rebalancing term <i>v</i> as: 
<div style="text-align:center"><img src="resources/rebalancingterm.png" /></div>
<div style="text-align:center"><img src="resources/pfactor.png" /></div>
<div style="text-align:center"><img src="resources/normalizedw.png" /></div>

* The prior distribution <i>p~</i>&nbsp; was calculated by first extracting the color contributions of each image. These contributions were derived by extracting the euclidean distances between the pixels and their 5 nearest neighbors in-gamut. These distances were then inversed because the smaller distances map to largest contributions. This process is similar to the effect that the Gaussian kernel has provided before.  After inversing, they were divided by their local total and put inside an appropriate cell in the output array. Keep in mind, this was done in local space. Afterwards, when the contributions were saved, each of them was divided against the total amount of contributions for the whole dataset, thus producing the priors of the dataset. This process can be found in the scripts `calc_contribs.py` and `calc_priors.py`.

* As mentioned above, to generate the class rebalancing term <i>w</i>, the prior distribution is mixed in with a uniform distribution. This makes sure that the prior factor generated later won't favor colors too disproportionately. Thus, by mixing with a distribution that provides equally distributed probabilities, this disproportion is rendered mute. Exactly what proportions of these distributions gets mixed is decided by the hyperparameter gamma <i>s</i> . 
```python
# It's obvious that gamma regulates the proportion of the uniform distribution included in the mix. 
mixed_distribution = (1 - gamma) * prior_distr + gamma * uniform_distr
```
* Afterwards, we exponentialize the mixed distribution, as seen in the formula above by -1. This is essentially the boost factor that will be multiplied with the encoded distances during backpropagation to favor specific colors. Because we exponentialize by -1, the boost factor will be significantly higher for the lowest values in the mixed distribution, which are essentially the more vibrant colors that we are trying to boost. Keep in mind, this needs to be normalized so that it matches the condition that the sum over the rebalancing term and the prior distribution must be equal to one.
```python
alpha = 1
prior_factor = mixed_distribution ** -alpha # This is w
prior_factor /= np.sum(prior_distr * prior_factor) # Normalizing w
```
* The prior factor then gets multiplied with the encodings generated from the previous layer, thus boosting specific values which are considered rare. These functionalities are implemented and can be found in the classes `PriorFactor` and `PriorBoostLayer`. 

One might think that we boost all values by doing this, but that is not completely correct. The boosting term <i>w</i> is not yet fully processed. If I were to just multiply the boost factors for each encoding with all encodings, this will essentialy also boost colors that are near 0. Those colors are grayish and shouldn't be boosted at all neither considered vibrant at all. To go around this, I've created an additional layer called `NonGrayMaskLayer`, which generates a mask containing 0's for the colors that are considered gray and 1's for the ones that are considered vibrant. The mask is generated by checking if the sum of the (a, b) pairs is greater than a given threshold. I've set the threshold to be 5, since the more obvious grayish colors are between -5 and 5. This mask then gets multiplied with the boost factors produced from the prior boost layer and intuitively, the colors that are considered grayish do not get boosted because they are multiplied with 0. The result of this operation is considered to be the prior factor <i>w</i>&nbsp; used in the loss function.


## 3. Network architecture

### 3.1 Pooling layers
An important feature that the network achitecture has is that it uses no pooling layers. The reason for this is that pooling is very 'destructive' towards its input data, plainly dropping information in the process of producing output. In tasks like image classification, this is not considered harmful, because the final output of the network is not directly proportional to the size of the input. However, in tasks like colorization or segmentation, using pooling layers can result in negative influence on final accuracy, as the resolution of the output is directly proportional to the size of the input. Instead of using pooling layers, downsampling is achieved by increasing the stride of some of the convolutional layers. This is a better fit for this task, as more information is preserved and can reach the later layers of the network. 

### 3.2 Model
The model that I train is identical to the one used by Zhang. et. al for natural image colorization on ImageNet. It is a model composed of stacked convolutional layers only. Each block of conv layers refers to a groupping of 2 to 3 convolutional layers followed by a rectified linear unit activation for each individual conv layer. Between blocks, a batch normalization layer is inserted to help prevend exploding or vanishing gradient problems and speed up convergence. All convolution layers learn filters of size 3x3, with the exception of the upsampling transposed convolution <b>conv8.1</b>, which uses 4x4 filters instead. In total, there are 22 convolutional layers split into 8 blocks, plus the prediction layer at the end. Initial layers contain a lower number of output channels, to simulate the use of a number of low level features, usually very similar to filters that are used by many corner or edge detection algorithms. The initial layers function as feature extractors, while the later layers have the role of information encoding. The resolution is quickly reduced to half of the input size and quarter of the input size after 2 or 4 layers. It is further reduced to one eight of the input size in the conv3 block and later upsampled back to one fourth by block <b>conv8</b> via learned upsampling filters.  One fourth of the input size is the total prediction output resolution. The model can be found in the script `model.py`.

## 4. Training

Just a quick side note -> I'm writing this paragraph on the 23rd of June, 1 day before the submition deadline. I've been training the network for 3 days now (and I'm even using my GPU) and its only been able to iterate over 2 epochs, so the information displayed below is only theoretically considered plausible by me, but in practice, I still don't know my end results. Loss seems to be decreasing though. My batch size is 4, because if I increase it, I get out of memory errors and training can't be performed...

Side Note: The training process can be found in the script `train.py`. 

### 4.1 Weight Initializations
I used the Xavier initialization as it is considered standard in the community and keeps the variance equal over all layers. It is supposed to provide higher convergence rates, which is essential in my case since training is taking too long.

### 4.2 Optimizer
Since Adam seems to be the go-to optimizing algorithm among researchers who use CNNs, I decided to follow suit in this matter. There are certain disadvantages to using Adam, such as increased memory usage, as it keeps at least two state variables for each weight - generally increasing memory requirements, which can cause issues if hardware is a limiting factor, bringing the need to reduce batch size in order to fit the network into the GPU VRam, but as stated in the literature, the convergence speed gains are significant when compared to plain SGD.

## 5. Results and experiments
As stated previously, I DID NOT manage to train the network fully, due to hardware limitations, I managed to train the network only 3 epochs, even with a reduced batch size. I did save the state dictionaries of the model as well as the optimizer to continue training further in time. Also, the loss functioned semeed to reduce over time which is a good hint. The images were also prone to produce better results after each epoch, however, training the network appropriately would need atleast 20-30 epochs of training, which, given my hardware limitations would require atleast 20 days. 
