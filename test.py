import torch
import numpy as np
import torch.nn.functional as F
import torch.nn as nn
import os
from torchvision import transforms
from skimage.transform import resize
from skimage import color
from model import ColorizationModel
from PIL import Image
from skimage.color import rgb2lab
import scipy
import argparse

scale_transform = transforms.Compose([
    transforms.Scale(256),
    transforms.RandomCrop(224),
])


def decode(data_l, conv8_313, rebalance=1):
    data_l = data_l[0] + 50
    data_l = data_l.cpu().data.numpy().transpose((1, 2, 0))
    conv8_313 = conv8_313[0]
    enc_dir = './resources'
    conv8_313_rh = conv8_313 * rebalance
    # print('conv8',conv8_313_rh.size())
    class8_313_rh = F.softmax(conv8_313_rh, dim=0).cpu().data.numpy().transpose((1, 2, 0))
    # np.save('class8_313.npy',class8_313_rh)
    class8 = np.argmax(class8_313_rh, axis=-1)
    # print('class8',class8.shape)
    cc = np.load(os.path.join(enc_dir, 'pts_in_hull_imagenet.npy'))
    # data_ab = np.dot(class8_313_rh, cc)
    data_ab = cc[class8[:][:]]
    # data_ab=np.transpose(data_ab,axes=(1,2,0))
    # data_l=np.transpose(data_l,axes=(1,2,0))
    # data_ab = resize(data_ab, (224, 224,2))
    data_ab = data_ab.repeat(4, axis=0).repeat(4, axis=1)

    img_lab = np.concatenate((data_l, data_ab), axis=-1)
    img_rgb = color.lab2rgb(img_lab)
    return img_rgb


def load_image(image_path, transform=None):
    image = Image.open(image_path)

    if transform is not None:
        image = transform(image)
    image_small = transforms.Scale(56)(image)
    image_small = np.expand_dims(rgb2lab(image_small)[:, :, 0], axis=-1)
    image = rgb2lab(image)[:, :, 0]
    image = torch.from_numpy(image).unsqueeze(0)
    return image, image_small




def main():
    data_dir = args.test_images
    dirs = os.listdir(data_dir)
    color_model = nn.DataParallel(ColorizationModel()).cuda().eval()
    color_model.load_state_dict(torch.load(args.state_dict))

    for file in dirs:
        image, image_small = load_image(data_dir + '/' + file, scale_transform)
        image = image.unsqueeze(0).float().cuda()
        img_ab_313 = color_model(image)
        out_max = np.argmax(img_ab_313[0].cpu().data.numpy(), axis=0)
        print('out_max', set(out_max.flatten()))
        color_img = decode(image, img_ab_313)
        # print(color_img)
        # break
        color_name = args.result_path + file
        scipy.misc.imsave(color_name, color_img * 255.)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test_images', type=str, default='./data/test', help='Specify path of dir with test images')
    parser.add_argument('--result_path', type=str, default='../results', help = 'Specify dir to save result images in')
    parser.add_argument('--state_dict', type=str, default='/saved_models/model-3-7500.ckpt', help = 'Specify state dict of model to load for testing')
    args = parser.parse_args()
    main()