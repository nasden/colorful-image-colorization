## Results
Unfortunately, 1 epoch of training took me 24 hours even when using my GPU. I only managed to train 3 epochs, due to the deadline
 being today and the lack of time so the results are bad, but the loss function is decreasing over each epoch and if trained properly (20 - 30 epochs), the network
 should produce pretty decent results. One batch contains only 4 images, otherwise I get an out of memory error when trying to train the network. I have uploaded the last state of the optimizer and the last state of the model, they can be found in the `states` folder.
 Here are some examples of the predictions on the test set: 
 
 ### Predicted
![](some_results/prediction1.png), ![](some_results/prediction2.png), ![](some_results/prediction3.png)

### Ground truth
![](some_results/groundtruth1.png), ![](some_results/groundtruth2.png), ![](some_results/groundtruth3.png)

## Usage 
To train the network on a dataset, run the script `train.py` by specifying the following:
```python
python train.py 
--train_images='path/to/training_data' 
--model_path='path/to/save/the/model/statedict'
--save_step='250' 
--num_epochs=NumOfEpochs 
--batch_size=NumOfBatches
--num_workers=NumOfThreads
--log_step=NumOfLogs
```

The `save_step` parameter regulates after how many batches does the state dictionary need to be saved to the `model_path`.
The log step parameter indicates after how many iterations should additional information (loss, epoch, batch) be printed
when training.The other parameters should be clear.   

If you want to train the network on the dataset that I used, there is a Dropbox link to the dataset underneath.  

If you want to test the network on unseen data, you'll have to run the script `test.py`. You  also need to load a state dictionary of the model you want to use on that data,
 the path where the results should be stored and the path of the unseen data. If you want to use the test set I've provided,
 refer to the aforementioned Dropbox link. 
  ```python
python test.py
--test_images='path/to/test/images'
--result_path='path/where/to/store/results'
--state_dict='path/to/statedict/of/model'
```

## Documentation
For any questions on how the task was completed, refer to the file `colorization-doc.md`

## Data
This is the data I generated and used for training and testing -> https://www.dropbox.com/sh/n2t1hzboco4ah95/AAA37tAZ3XeGaKYK5p7P0n39a?dl=0

## References
[1] https://arxiv.org/pdf/1603.08511.pdf <br/>
[2] https://github.com/richzhang/colorization <br/>
[3] http://conteudo.icmc.usp.br/pessoas/moacir/papers/Ponti_QuantDimensionalityReduction_Preprint.pdf<br/>
[4] https://github.com/cyanamous/Color-Quantization-using-K-Means/blob/master/Color%20Quantization%20Using%20K-Means.ipynb<br/>
[5] https://www.datascience.com/blog/introduction-to-bayesian-inference-learn-data-science-tutorials <br/>

