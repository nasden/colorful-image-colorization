import os
from torchvision import datasets, transforms
from skimage import io
import torch.utils.data as data
import torch
import torch.nn as nn
from PIL import Image
import os
from skimage.color import rgb2lab, rgb2gray, lab2rgb
import numpy as np
import matplotlib.pyplot as plt
import argparse
from data_loader import TrainImageFolder
from train_layers import QuantizedEncLayer, PriorBoostLayer, NonGrayMaskLayer
from model import ColorizationModel
original_transform = transforms.Compose([
    transforms.RandomCrop(224),
    transforms.RandomHorizontalFlip(),
])

def main(args):

    train_set = TrainImageFolder(args.train_images, original_transform)
    data_loader = torch.utils.data.DataLoader(train_set, batch_size = args.batch_size, shuffle = True, num_workers = args.num_workers)

    enc_layer = QuantizedEncLayer()
    boost_layer = PriorBoostLayer()
    nongraymask_layer = NonGrayMaskLayer()
    model = nn.DataParallel(ColorizationModel()).cuda()

    criterion = nn.CrossEntropyLoss(reduce=False).cuda()
    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, lr=args.learning_rate)
    
    total_step = len(data_loader)
    for epoch in range(args.num_epochs):
        for i, (images, img_ab) in enumerate(data_loader):
            try:
                images = images.unsqueeze(1).float().cuda()
                img_ab = img_ab.float()

                encoded, max_encoded = enc_layer.forward(img_ab)
                targets = torch.Tensor(max_encoded).long().cuda()

                boosts = torch.Tensor(boost_layer.forward(encoded)).float().cuda()
                mask = torch.Tensor(nongraymask_layer.forward(img_ab)).float().cuda()

                boost_nongray = boosts * mask

                outputs = model(images)
                output = outputs[0].cpu().data.numpy()

                out_max=np.argmax(output,axis=0)

                loss = (criterion(outputs, targets) * (boost_nongray.squeeze(1))).mean()

                model.zero_grad()
                loss.backward()
                optimizer.step()

                if i % args.log_step == 0:
                    print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'
                          .format(epoch, args.num_epochs, i, total_step, loss.item()))

                if (i + 1) % args.save_step == 0:
                    torch.save(model.state_dict(), os.path.join(args.model_path, 'model-{}-{}.ckpt'.format(epoch + 1, i + 1)))
                    torch.save(optimizer.state_dict(), os.path.join("/home/nasdenkov/PycharmProjects/colorful-image-colorization/optimizerstates", 'state-{}-{}.ckpt').format(epoch + 1, i + 1))
            except:
                pass




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, default='/home/nasdenkov/PycharmProjects/colorful-image-colorization/saved_models', help='path for saving trained models')
    parser.add_argument('--train_images', type = str, default = '/home/nasdenkov/colorization_data/data/rgb/train', help = 'path to images for training')
    parser.add_argument('--save_step', type= int, default = 250, help = 'step size to make checkpoints of model when training')
    parser.add_argument('--log_step', type = int, default = 1, help = 'step size to print log information')
    # Model parameters
    parser.add_argument('--num_epochs', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=4)
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--learning_rate', type=float, default=1e-3)
    args = parser.parse_args()
    print(args)
    main(args)